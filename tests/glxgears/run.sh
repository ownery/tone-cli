run()
{
	users=(`users`)
	#xhost + 必须从有显示连接的用户运行，即登陆图形化界面的用户下运行
	export DISPLAY=:0.0
	sudo -u ${users[0]} xhost +
	timeout -k 10s $t sh $TONE_BM_SUITE_DIR/vblank.sh
	echo running finish---------------
}

teardown()
{
	echo 'teardown'
}


parse()
{
	j=0
	total=0
	for i in $($TONE_BM_SUITE_DIR/parse.awk)
	do
		j=`expr $j + 1`
		total=$(echo "$total + $i"|bc)
	done
	average=$(echo "scale=3;$total/$j"|bc)
	echo "time_$t: ${average}FPS"
}
