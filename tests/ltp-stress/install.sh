GIT_URL="https://gitee.com/anolis/ltp.git"

if echo "ubuntu debian uos kylin" | grep $TONE_OS_DISTRO; then
	DEP_PKG_LIST="pkg-config libtirpc-dev autoconf m4 genisoimage sysstat automake gcc psmisc libaio-dev unzip dosfstools keyutils net-tools bc numactl libcap-dev quota libmnl libmnl-dev"
else
	DEP_PKG_LIST="libtirpc-devel autoconf m4 genisoimage sysstat automake gcc psmisc libaio-devel unzip dosfstools keyutils net-tools bc numactl-devel libcap-devel quota dnsmasq libmnl-devel libmnl"

fi

if [ -z "$LTP_BRANCH" ]; then
	BRANCH="master"
	kver=$(uname -r | cut -d. -f1)
	[[ $kver -le 4 ]] && BRANCH="anck-4.19"
	[[ $kver -eq 5 ]] && BRANCH="anck-5.10"
else
	BRANCH=$LTP_BRANCH
fi

build()
{
	export CFLAGS+="-fcommon"
	make autotools
	./configure --prefix="$TONE_BM_RUN_DIR"
	make
}

install()
{
	make install
}
