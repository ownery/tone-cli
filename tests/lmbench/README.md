# LMbench - Tools for performance analysis

## Description

LMbench is: 
* Suite of simple, portable benchmarks
* Compares different unix systems performance
* Results available for most major vendors (SUN, HP, IBM, DEC, SGI, PCs including 200 Mhz P6's)
Free software, covered by the GNU General Public License. 
* Bandwidth benchmarks
  * Cached file read
  * Memory copy (bcopy)
  * Memory read
  * Memory write
  * Pipe
  * TCP
* Latency benchmarks
  * Context switching.
  * Networking: connection establishment, pipe, TCP, UDP, and RPC hot potato
  * File system creates and deletes.
  * Process creation.
  * Signal handling
  * System call overhead
  * Memory read latency
* Miscellanious
  * Processor clock rate calculation

## Category
performance

## Parameters
* testcase: predefined subsets of benchmarks.
* nr_task: number of threads to use.

## Results
read_bandwidth: 4165.88 MB/s open2close_bandwidth: 4168.63 MB/s
