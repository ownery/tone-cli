#!/bin/bash

pass()
{
	echo "====PASS: $*"
}

fail()
{
	echo "====FAIL: $*"
}

warn()
{
	echo "====WARN: $*"
}

skip()
{
	echo "====SKIP: $*"
}

anck_build()
{
  build_script=$TONE_BM_SUITE_DIR/anck_build.sh
  python $TONE_BM_SUITE_DIR/anck_build.py $build_script
	upload_archives $(find /anck_build/ck-build/outputs -name *.rpm)
	return 0
}

anck_boot_test()
{
	[ -n "$REMOTE_HOST" ] || {
		echo "Error: REMOTE_HOST is not set"
		return 1
	}

	anck_rpms_dir="/anck_build/ck-build/outputs/0"
	kver_new=$(find $anck_rpms_dir -name kernel-headers*.rpm | \
		head -n 1 | xargs \
		rpm -qp --queryformat="%{VERSION}-%{RELEASE}.%{ARCH}\n")

	remote_check || return 1
	remote_cmd "rm -rf /anck_rpms"
	logger "scp -r $anck_rpms_dir root@$REMOTE_HOST:/anck_rpms"
	remote_cmd "rpm -Uvh --force /anck_rpms/*.rpm"
	remote_cmd "reboot"
	sleep 30

	[ -n "$REBOOT_TIMEOUT" ] || REBOOT_TIMEOUT=600
	wait_time=30
	ret=1
	while [ "$wait_time" -lt "$REBOOT_TIMEOUT" ]
	do
		remote_check | grep -q $kver_new && ret=0 && break
		echo "Still waiting boot..."
		wait_time=$((wait_time + 30))
		sleep 30
	done
	remote_check
	if [ "$ret" -ne 0 ]; then
		echo "Error: failed to boot $kver_new in ${REBOOT_TIMEOUT}s"
	else
		echo "Succeed to boot new kernel!"
	fi
	return $ret
}

remote_check()
{
	logger "ping -c 1 $REMOTE_HOST" || return 1
	logger "timeout 5 ssh root@$REMOTE_HOST uname -r" || return 1
	return 0
}

check_dmesg()
{
	local dmesg_info=$(remote_cmd "dmesg -l err -T | grep -v '\/etc\/keys\/x509_'" | grep -v dmesg)
	if ! [ -z "${dmesg_info}" ]; then
		echo "Got err from dmesg"
		return 1
	else
		echo "Check dmesg success."
		return 0
	fi
}

remote_cmd()
{
	logger "ssh root@$REMOTE_HOST $*"
}

run()
{
	anck_build_dir="/anck_build"
	[ -d "$anck_build_dir" ] || mkdir -p $anck_build_dir
	# remove anck build log before build
	rm -f /tmp/anck_*.log
	# build ANCK
	anck_build
	# archive build logs
	upload_archives /tmp/anck_allyesconfig.log
	upload_archives /tmp/anck_allnoconfig.log
	upload_archives /tmp/anck_defconfig.log
	upload_archives /tmp/anck_rpmbuild.log
	upload_archives /tmp/anck_debugconfig.log
	upload_archives /tmp/anck_checkconfig.log
	# check result
	if tail -n 5 /tmp/anck_checkconfig.log | grep check_kconfig:\ pass; then
		pass "check_Kconfig"
	else
		warn "check_Kconfig"
	fi
	if tail -n 5 /tmp/anck_allyesconfig.log | grep allyesconfig:\ pass; then
		pass "build_allyes_config"
	else
		fail "build_allyes_config"
	fi
	if tail -n 5 /tmp/anck_allnoconfig.log | grep allnoconfig:\ pass; then
		pass "build_allno_config"
	else
		fail "build_allno_config"
	fi
	if tail -n 5 /tmp/anck_defconfig.log | grep anolis_defconfig:\ pass; then
		pass "build_anolis_defconfig"
	else
		fail "build_anolis_defconfig"
	fi
	if tail -n 5 /tmp/anck_debugconfig.log | grep debug_defconfig:\ pass; then
		pass "build_anolis_debug_defconfig"
	else
		fail "build_anolis_debug_defconfig"
	fi
	if tail -n 5 /tmp/anck_rpmbuild.log | grep rpmbuild:\ pass; then
		pass "anck_rpm_build"
		if anck_boot_test; then
			pass "boot_kernel_rpm"
		else
			fail "boot_kernel_rpm"
			return 1
		fi
	else
		fail "anck_rpm_build"
		skip "boot_kernel_rpm"
		return 1
	fi
	return 0
}

parse()
{
	$TONE_BM_SUITE_DIR/parse.awk
}
