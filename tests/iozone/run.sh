#!/bin/sh
[[ ! $IOZONE_FILEPATH ]] && filepath="/home" || filepath=$IOZONE_FILEPATH
testfile=$filepath/iozone.data

run()
{
    available_disk_kb=$(df $filepath | tail -1 | awk '{print $4}')
    [ ! $available_disk_kb ] && exit 1
    memory_size_kb=$(free | grep Mem | awk '{print $2}')
    if [ "$filesize" = "200%" ];then
        test_file_kb=$(($memory_size_kb * 2))
        memory_size_gb=$(($memory_size_kb * 2 / 1024 / 1024))
        filesize="$memory_size_gb"G
    else
        test_file_gb=$(echo "$filesize" | awk -F 'G' '{print $1}')
        test_file_kb=$(expr $test_file_gb \* 1024 \* 1024)
    fi
    if [ $test_file_kb -le $available_disk_kb ];then
        cd $TONE_BM_RUN_DIR/iozone3_492/src/current/
        logger ./iozone -a -s ${filesize} -r ${recordsize} -f $testfile
        echo "$(date) iozone end ${filesize}"
        logger sync
        logger "echo 3 > /proc/sys/vm/drop_caches"
        sleep 30
    else
        echo "ERROR:Insufficient system disk space"
        [ $test_file_kb -le $available_disk_kb ]
        exit 1
    fi
}

teardown()
{
    rm -f $testfile || true
}


parse()
{
    $TONE_BM_SUITE_DIR/parse.awk
}
