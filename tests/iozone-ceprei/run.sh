run()
{
    cd $TONE_BM_RUN_DIR/iozone3_430/src/current
	free > data_tmp
 	storage=0
 	for line in $(cat data_tmp)
 	do
		case "$line"
		in [1-9][0-9]*)
		storage=$line
		break
		;;
		*)
		;;
		esac
	done
	a=1024
	internal=`expr $storage / $a / 1024`

	internal_storage=$(echo "scale=5; ${storage}/${a}/1024" | bc)

	if [ `expr $internal_storage \> $internal` -eq 1 ]
	then
		internal_storage=`expr $internal + 1`
	fi
	if [ `expr $s \> 1` -eq 1 ]; then
		internal_storage=`expr $internal_storage \* $s`"g"
	else
		internal_storage=$(echo "${internal_storage} * ${s}" | bc)
		internal_storage=${internal_storage}"g"
	fi
	echo "internal_storage: $internal_storage"
	./iozone -s $internal_storage -i $i1 -i $i2 -i $i3 -r $r -f /test_iozone
	rm -rf data_tmp
	cd ..//..//..

}

teardown()
{
	echo "run complete"
}


parse()
{
	$TONE_BM_SUITE_DIR/parse.awk
}
