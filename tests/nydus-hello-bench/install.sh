GIT_URL=("https://github.com/nydusaccelerator/hello-bench.git" "https://github.com/dragonflyoss/image-service.git" "https://github.com/containerd/nydus-snapshotter.git")
hello_bench_branch=${hello_bench_branch:-"main"}
image_service_branch=${image_service_branch:-"master"}
nydus_snapshotter_branch=${nydus_snapshotter_branch:-"main"}

extract_src()
{
    set_git_clone
    git_clone_exec $TONE_BM_CACHE_DIR/hello-bench $TONE_BM_RUN_DIR/hello-bench --branch "${hello_bench_branch}"
    git_clone_exec $TONE_BM_CACHE_DIR/image-service $TONE_BM_RUN_DIR/image-service --branch "${image_service_branch}"
    git_clone_exec $TONE_BM_CACHE_DIR/nydus-snapshotter $TONE_BM_RUN_DIR/nydus-snapshotter --branch "${nydus_snapshotter_branch}"
}

build()
{
    :
}

install()
{
    pip3 install pandas
}
