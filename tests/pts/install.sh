PTS_URL="https://ostester.oss-cn-hangzhou.aliyuncs.com/benchmarks/phoronix-test-suite-10.8.4.tar"
PTS_CACHE="https://ostester.oss-cn-hangzhou.aliyuncs.com/benchmarks/pts-download-cached.tar"

DEP_PKG_LIST="php php-xml php-cli zlib-devel nmap perl-IPC-Cmd python3 expat-devel"

grep -q Anolis /etc/os-release && DEP_PKG_LIST+=" php-json nmap-ncat"


fetch()
{
        echo "Download phoronix-test-suite-10.8.4.tar"
        wget -t 3 -T 300 --no-clobber $PTS_URL
        echo "Download pts-download-cached.tar"
        wget -t 3 -T 600 --no-clobber $PTS_CACHE
}

check_install_path()
{
        [[ -n $PTS_TESTS_INSTALL_DIR ]] || PTS_TESTS_INSTALL_DIR=$TONE_BM_RUN_DIR
        free_install_disk=$(df -BG $PTS_TESTS_INSTALL_DIR | tail -1 | awk '{print $4}'  | sed 's/G//')
        if [[ $free_install_disk ]];then
            [[ "$free_install_disk" -lt "20" ]] && { echo "ERROR:Insufficient disk space for $PTS_TESTS_INSTALL_DIR" ;exit 1;}
        else
            echo "$PTS_TESTS_INSTALL_DIR: No such file or directory"
        fi
}

build()
{
        check_install_path

}


install()
{
        logger "tar xvf $TONE_BM_CACHE_DIR/phoronix-test-suite-10.8.4.tar -C $PTS_TESTS_INSTALL_DIR"
        logger "tar xvf $TONE_BM_CACHE_DIR/pts-download-cached.tar -C $PTS_TESTS_INSTALL_DIR"
        if [[ -d $PTS_TESTS_INSTALL_DIR ]];then
             mkdir -p $PTS_TESTS_INSTALL_DIR/{installed-tests,test-results}
        fi
        cd $PTS_TESTS_INSTALL_DIR/phoronix-test-suite
        logger ./phoronix-test-suite user-config-set \
                EnvironmentDirectory=$PTS_TESTS_INSTALL_DIR/installed-tests \
                CacheDirectory=$PTS_TESTS_INSTALL_DIR/download-cache \
                ResultsDirectory=$PTS_TESTS_INSTALL_DIR/test-results \
                SaveResults=TRUE \
                OpenBrowser=FALSE \
                UploadResults=FALSE \
                PromptForTestIdentifier=FALSE \
                PromptForTestDescription=FALSE \
                PromptSaveName=FALSE \
                RunAllTestCombinations=FALSE \
                Configured=TRUE
}

