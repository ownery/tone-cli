GIT_URL="https://gitee.com/mirrors_axboe/fio.git"
BRANCH="fio-3.19"

if echo "ubuntu debian uos kylin" | grep $TONE_OS_DISTRO; then
    DEP_PKG_LIST="libaio1 libc6 libibverbs1 librados2 librbd1 librdmacm1 zlib1g zlib1g-dev"
else
    DEP_PKG_LIST="libaio-devel clang liburing"
fi
build()
{
    # remove system version fio
    if echo "ubuntu debian uos kylin" | grep $TONE_OS_DISTRO; then
        dpkg -l | grep -q fio && dpkg -r fio
    else
        rpm -qa | grep -q fio && rpm -e fio
    fi

    # build fio from source code
    ./configure --prefix=$TONE_BM_RUN_DIR
    make

}

install()
{
    make install
}

uninstall()
{
    umount_fs
}

