#!/usr/bin/env python
# -*- coding: utf-8 -*-


from utils.log import logger
from module_interfaces import JOBPRE


class UploadTestResult(JOBPRE):
    def run(self, job_instance):
        logger.debug("Upload tests result")
